# P20 (portfolio) 

## Init progect

* Clone repo
* launch npm install

__________________________________

## Gulp task

Use gulp task:

* gulp serve (serve bundle project)
* gulp build (bundle project on dist folder - local path)
* gulp remote (bundle project on dist folder - remote path)
* gulp prod (bundle project on dist folder - production path)

__________________________________

## Remote and Production folder for assets

If needed set up remote and production folders edit gulpfile.js :

	const stageUrl;
	const prodUrl;

And to use different assets paths edit gulpfile.js:

	const imagesPath;
	const scriptsPath;
	const stylesPath;

__________________________________

## Version / cache

Css and js bundle are versioned with some attributes of package.json: lang and version

	const replaceVersion = lang + '_' + version;

See function html()

__________________________________

## Locale

If you need some locale translate, use json inside 'locale' folder.

Gulpfile.js read json based on

	const localePath = './app/locale/'+ lang +'.json';

Data is loaded into pug view, where you can simply use base interpolation to print out values

## What's in there?

###Pug [website](https://pugjs.org)

HTML templating engine

__________________________________

### Sass [website](https://sass-lang.com/)

CSS extension language

__________________________________

### Lazysizes [github](https://github.com/aFarkas/lazysizes)

A lazy loader for resources (images, iframes, etc...)




