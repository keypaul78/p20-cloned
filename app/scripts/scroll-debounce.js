/*
** ========================================================================
**  Scroll debounce
** ========================================================================
	Scroll event request based on RAF
** ========================================================================
*/
_R.scroll = (function (_R){
	'use strict';
	const config = {
		latestKnownScrollY: 0,
		ticking: false
	};

	const getCurrentScroll = function(){
		return config.latestKnownScrollY;
	}

	const updateScroll = function() {
		config.ticking = false;
		config.latestKnownScrollY = getScrollPosition();
		let currentScrollY = config.latestKnownScrollY;
	};

	const requestTickScroll = function() {
		if(!config.ticking) {
			requestAnimationFrame(updateScroll);
		}
		config.ticking = true;
	};

	const getScrollPosition = function(){
		return window.scrollY ||
		document.body.scrollTop ||
		document.documentElement.scrollTop || 0;
	}

	//auto init
	const init = (function(){
		window.addEventListener('scroll', requestTickScroll);
	})();

	//public
	return {
		init: init,
		getCurrentScroll: getCurrentScroll
	};

})(_R);
