/*
** ========================================================================
**  Nav
** ========================================================================
	Behavior of main menu
** ========================================================================
*/
_R.navigation = (function(_R){
	'use strict';
	/*
		CONSTANTS
	*/
	const CONFIG = {
		navElm : 				document.querySelector('.float-nav'),
		classHide:  			'hide',
		firstScroll: 			null, 
		gapScroll: 				0,
		isScrolling: 			false,
		isChecking: 			false,
		timer: 					null,
		checkTime: 				200,
		gapShadow: 				80,
		classShadow: 			'shadow-nav'
	};

	/*
		METHODS
	*/
	const setNavShadow = function() {
		if(_R.scroll.getCurrentScroll() > CONFIG.gapShadow){
			CONFIG.navElm.classList.add(CONFIG.classShadow);
		}else {
			CONFIG.navElm.classList.remove(CONFIG.classShadow);
		}
	};

	const setNavClass = function(){
		let gapY = CONFIG.firstScroll - _R.scroll.getCurrentScroll();
		if(gapY < 0){
			if(!CONFIG.navElm.classList.contains(CONFIG.classHide)){
				CONFIG.navElm.classList.add(CONFIG.classHide);
			}
		}else {
			CONFIG.navElm.classList.remove(CONFIG.classHide)
		}
		CONFIG.isChecking = false;
		CONFIG.isScrolling = false;
	}

	const checkScrolling = function(scrollY){
		if(!CONFIG.isScrolling){
			CONFIG.isScrolling = true;
			CONFIG.firstScroll = _R.scroll.getCurrentScroll();
		}else {
			if(!CONFIG.isChecking){
				CONFIG.isChecking = true;
				CONFIG.timer = setTimeout(setNavClass, CONFIG.checkTime);
			}
		}
		setNavShadow();
	};

	/*
		AUTO INIT
	*/
	const init = (function () {
		window.addEventListener('scroll', checkScrolling);
	})();

	/*
		METHOD GOES PUBLIC
	*/
	return {
	};

})(_R);


