/*
** ========================================================================
**  Collpase
** ========================================================================
	Show / hide collapsed elms
** ========================================================================
*/
_R.collapse = (function(_R){
	'use strict';
	/*
		CONSTANTS
	*/
	const CONFIG = {
		selector : 				'[data-toggle="collapse"]',
		data: 					'data-target',
		classExpanded:  		'active',
		classExpandedParent: 	'expanded',
		bodyClassOpen: 			'nav-open',
		bodyClassOverflow: 		'full-overflow',
		collapseDuration: 		800,
		tReset: 				null

	};

	/*
		METHODS
	*/
	const toggle = function(e){
		e.preventDefault();
		let el = this;
		if(!el.classList.contains(CONFIG.classExpanded)){
			show(el);
		}else {
			hide(el);
		}
	};

	const show = function(el){
		window.clearTimeout(CONFIG.tReset);
		el.collapsedEl.classList.add(CONFIG.classExpanded);
		el.classList.add(CONFIG.classExpanded);
		el.setAttribute('aria-expanded', true);
		el.parentNode.classList.add(CONFIG.classExpandedParent);
		document.body.classList.add(CONFIG.bodyClassOpen);
		document.body.classList.add(CONFIG.bodyClassOverflow);
	};

	const hide = function(el){
		el.collapsedEl.classList.remove(CONFIG.classExpanded);
		el.classList.remove(CONFIG.classExpanded);
		el.setAttribute('aria-expanded', false);
		el.parentNode.classList.remove(CONFIG.classExpandedParent);
		document.body.classList.remove(CONFIG.bodyClassOpen);
		CONFIG.tReset = setTimeout(function () {
			document.body.classList.remove(CONFIG.bodyClassOverflow);
		}, CONFIG.collapseDuration);
		el.blur();
	};

	/*
		AUTO INIT
	*/
	const init = (function () {

		let elements = document.querySelectorAll(CONFIG.selector);
		let i = 0;
		for (i = elements.length; i--;) {
		  let el = elements[i];
		  let dataTarget = el.getAttribute(CONFIG.data);
		  el.collapsedEl = document.querySelector(dataTarget);
		  el.addEventListener('click' , toggle);
		}
	})();

	/*
		METHOD GOES PUBLIC
	*/
	return {
		
	};

})(_R);


