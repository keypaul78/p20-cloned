/*
** ========================================================================
**  Theme
** ========================================================================
	Easy theme dark/light switch
** ========================================================================
*/


_R.theme = (function(_R){
	'use strict';
	/*
		CONSTANTS
	*/
	const CONFIG = {
		body:  					document.body,
		toggler: 				'[data-theme-toggler]',
		setter: 				'data-theme',
		darkClass: 				'dark-mode',
		controlsShow:         	'show',
		switcher: 				document.querySelector('#theme-switcher'),
		switchOpen: 			'open',
		switchClose: 			'close',
		timeSwitch: 				600,
		themeControl: 			document.querySelector('#theme-control'),
		tReset: 				null
	};

	/*
		METHODS
	*/

	const checkTheme = function(e){
		e.preventDefault();
		let el = this;
		let theme = el.getAttribute('data-theme');
		setTheme(theme);
	};

	const setTheme = function(theme){
		if(theme === CONFIG.darkClass){
			switchTheme(theme);
		}else{
			unsetDarkMode();
		}
	};

	const toggleTheme = function(e){
		e.preventDefault();
		let el = this;
		let theme = null;
		
		if(!CONFIG.body.classList.contains(CONFIG.darkClass)){
			theme = CONFIG.darkClass
		}
		document.querySelector('.dot-switcher').classList.add('switching');
		switchTheme(theme);
	};

	const switchTheme = function(theme){
		CONFIG.switcher.classList.add(CONFIG.switchOpen);
		CONFIG.tReset = setTimeout(function () {
			CONFIG.switcher.classList.add(CONFIG.switchClose);
			document.querySelector('.dot-switcher').classList.remove('switching');
			if(theme === CONFIG.darkClass){
				setDarkMode();
			}else{
				unsetDarkMode();
			}
			setTimeout(function () {
				CONFIG.switcher.classList.remove(CONFIG.switchClose);
				CONFIG.switcher.classList.remove(CONFIG.switchOpen);
			}, CONFIG.timeSwitch);
		}, CONFIG.timeSwitch);

	};

	const setDarkMode = function(){
		CONFIG.body.classList.add(CONFIG.darkClass);
		CONFIG.themeControl.classList.add(CONFIG.controlsShow);
		localStorage.setItem('roughdesign_choice-mode','dark');
	};

	const unsetDarkMode = function(){
		CONFIG.body.classList.remove(CONFIG.darkClass);
		CONFIG.themeControl.classList.add(CONFIG.controlsShow);
		localStorage.setItem('roughdesign_choice-mode','light');
	}

	/*
		AUTO INIT
	*/
	const init = (function () {
		let togglers = document.querySelectorAll(CONFIG.toggler);
		let i;
		for (i = togglers.length; i--;) {
		  let el = togglers[i];
		  el.addEventListener('click' , toggleTheme);
		}

		let setters = document.querySelectorAll('[' + CONFIG.setter + ']');
		let k;
		for (k = setters.length; k--;) {
		  let el = setters[k];
		  el.addEventListener('click' , checkTheme);
		}

	})();


	/*
		METHOD GOES PUBLIC
	*/
	return {
		setTheme: setTheme
	};

})(_R);


