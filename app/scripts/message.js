/*
** ========================================================================
**  Message 
** ========================================================================
	Conversational UI element based on local storage
** ========================================================================
*/
 // Some refactoring to do

_R.message = (function(_R){
	'use strict';
	/*
		CONSTANTS
	*/
	const CONFIG = {
		selectorData : 			'[data-message]',
		selectorMessage: 		'span',
		classActive: 			'active',
		classOpen:  			'open',
		classClose: 			'close',
		classBubbling: 			'bubbling',
		classDone: 				'done', 
		dataTime: 				'data-time',
		selectorChoice: 		'[data-choice] button',
		dataCallback:           'dat-callback',
		switchTime: 			800,
		wrapMessages: 			null,
		messages: 				null,
		interval: 				null,
		current:  				null
	};

	/*
		METHODS
	*/
	const closeMessagges = function(){
		CONFIG.messages[0].parentNode.classList.remove(CONFIG.classBubbling);
		CONFIG.wrapMessages.classList.remove(CONFIG.classOpen);
		CONFIG.wrapMessages.classList.add(CONFIG.classClose);

		activeScroll(0);
	};

	const activeScroll = function(delay){
		setTimeout(function(){
			let el = document.querySelector('.init-scroll');
			if(!!el){
				el.classList.add('active');
			}
		}, delay);	
	};

	const switchMessage = function(){

		CONFIG.messages[CONFIG.current].parentNode.classList.add(CONFIG.classBubbling);
		CONFIG.current++;
	
		if(CONFIG.current >= CONFIG.messages.length){
			closeMessagges();
			return;
		}

		if(CONFIG.messages[CONFIG.current].getAttribute('data-result') === null){
			CONFIG.interval = setTimeout(setMessage, CONFIG.switchTime);
		}else{
			CONFIG.interval = setTimeout(setMessage, 600);
		}
	
	};


	const setMessage = function(){

		let i = 0;
		for (i = CONFIG.messages.length; i--;) {
		  let el = CONFIG.messages[i];
		  el.classList.remove(CONFIG.classActive);
		  if(i < CONFIG.current){
		  	el.classList.add(CONFIG.classDone);
		  }
		}

		CONFIG.messages[CONFIG.current].classList.add(CONFIG.classActive);
		const firstViewTime = CONFIG.current === 0 ? 2000 : 0;		
		const messageTime = CONFIG.messages[CONFIG.current].getAttribute(CONFIG.dataTime) * 1000 + firstViewTime;
		CONFIG.messages[CONFIG.current].parentNode.classList.remove(CONFIG.classBubbling);

		if(CONFIG.messages[CONFIG.current].getAttribute('data-choice') === null){
			CONFIG.interval = setTimeout(switchMessage, messageTime);
		}

		
	}


	const setChoice = function(e){
		e.preventDefault();
		let el = this;
		let parent = this.parentNode;
		let target = document.querySelector(parent.getAttribute('data-target'));
		let choice = el.getAttribute('data-value');

		let btns = parent.querySelectorAll('button');
		let i = 0;
		for (i = btns.length; i--;) {
			let btn = btns[i];
			btn.classList.add('hide');
		}
		
		el.classList.remove('hide');
		el.classList.add('selected');

		target.querySelector('[data-value="' + choice + '"]').classList.add('selected');

		setwebStorage(parent.getAttribute('data-target').replace('#','') , choice);
		switchMessage();

	};


	const checkWebStorage = function(){
		let storageName = localStorage.getItem('roughdesign_name');
		if(storageName === null){
			initStorage();
		}else {
			readStorage();
		}
	};

	const initStorage = function(){
		localStorage.setItem('roughdesign_name', 'wizardBot');

		CONFIG.current = 0;//HELLO sentences
		CONFIG.wrapMessages = document.querySelector(CONFIG.selectorData);
		CONFIG.wrapMessages.setAttribute('data-message', 0);
		CONFIG.wrapMessages.classList.remove(CONFIG.classClose);
		CONFIG.wrapMessages.classList.add(CONFIG.classOpen);
		CONFIG.messages = CONFIG.wrapMessages.querySelectorAll(CONFIG.selectorMessage);
		CONFIG.messages[CONFIG.current].classList.add(CONFIG.classActive);


		let buttons = document.querySelectorAll(CONFIG.selectorChoice);
		let i = 0;
		for (i = buttons.length; i--;) {
		  let el = buttons[i];
		  el.addEventListener('click' , setChoice);
		}
		setMessage();

	};

	const setwebStorage = function(name, val){
		localStorage.setItem('roughdesign_' + name, val);
	};

	const readStorage = function(){
		activeScroll(1000);

		let musicChoice = localStorage.getItem('roughdesign_choice-music');
		let themeChoice = localStorage.getItem('roughdesign_choice-mode');

		_R.theme.setTheme(themeChoice + '-mode');
		_R.music.setMusic(musicChoice);

	};
	
	/*
		AUTO INIT
	*/
	const init = (function () {

		checkWebStorage();

	})();

	/*
		METHOD GOES PUBLIC
	*/
	return {
		setwebStorage: setwebStorage
	};

})(_R);


