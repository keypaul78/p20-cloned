/*
** ========================================================================
**  General Utils functions/methods
** ========================================================================

** ========================================================================
*/

_R.Utils = (function (_R){

	const CONFIG = {
		body: document.querySelector('body'),
		IE: 0
	};

	//check for IE
	const detectIE = function() {
		let ua = window.navigator.userAgent;
		let msie = ua.indexOf('MSIE ');
		if (msie > 0) {
			// IE 10 or older => return version number
			return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
		}
		let trident = ua.indexOf('Trident/');
			if (trident > 0) {
			// IE 11 => return version number
			let rv = ua.indexOf('rv:');
			return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
		}
		let edge = ua.indexOf('Edge/');
			if (edge > 0) {
			// Edge (IE 12+) => return version number
			return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
		}
		// other browser
		return false;
	};

	const detectIOS = function(){
		let userAgent = window.navigator.userAgent;
		if (userAgent.match(/iPad/i) || userAgent.match(/iPhone/i)) {
		   return 'ios';
		}
		return false;
	};


	//check touches
	const isTouchDevice = function () {
		return !!('ontouchstart' in window);
	};

	const isInViewport = function(element) {
		
	  let rect = element.getBoundingClientRect();
	  let html = document.documentElement;
	  return (
	    rect.top >= 0 &&
	    rect.left >= 0 &&
	    rect.bottom <= (window.innerHeight || html.clientHeight) &&
	    rect.right <= (window.innerWidth || html.clientWidth)
	  );
	 
	};

	const getOffset = function( el ) {
	    let _x = 0;
	    let _y = 0;
	    while( el && !isNaN( el.offsetLeft ) && !isNaN( el.offsetTop ) ) {
	        _x += el.offsetLeft - el.scrollLeft;
	        _y += el.offsetTop - el.scrollTop;
	        el = el.offsetParent;
	    }
	    return { top: _y, left: _x };
	}

	const getRect = function(element) {
	  let rect = element.getBoundingClientRect();
	  return rect;
	  
	};

	const getViewportSize = function(){
		let w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
		let h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
		let viewportSize = {
			w : w,
			h: h
		}
		return viewportSize;
	};

	const getComputedStyle = function(el){
		let style = el.currentStyle || window.getComputedStyle(el);
		return style;
	}

	const findAncestorByClass = function(el, cls) {
	    while ((el = el.parentElement) && !el.classList.contains(cls));
    	return el;
	}

	const stripPx = function(string){
		return string.replace('px','');
	};

	//flag for drag/swipe
	document.addEventListener('touchmove', function(e){
		_R.CONFIG.isTouchMove = true;
	});
	document.addEventListener('touchstart', function(e){
		if (event.originalEvent !== undefined && event.cancelable) {
			e.preventDefault();
		}
		_R.CONFIG.isTouchMove = false;
	});

	//auto init
	const init = (function () {
		if( isTouchDevice() ){
			CONFIG.body.classList.add('touch');
		}
		if( detectIOS()){
			CONFIG.body.classList.add('ios');
		}
		CONFIG.IE = detectIE();
		if(CONFIG.IE !== false){
			CONFIG.body.classList.add('ie');
		}
	})();

	//pubic
	return {
		init: 					init,
		isInViewport: 			isInViewport,
		isTouchDevice: 			isTouchDevice,
		getViewportSize : 		getViewportSize,
		findAncestorByClass: 	findAncestorByClass,
		getComputedStyle: 		getComputedStyle,
		stripPx: 				stripPx,
		getRect: 				getRect,
		getOffset: 				getOffset
	};
})(_R);