/*
** ========================================================================
**  Init state
** ========================================================================
	Ghost modal purge and nav toggle
** ========================================================================
*/


_R.init = (function(_R){
	'use strict';
	/*
		CONSTANTS
	*/
	const CONFIG = {
		selectorModal : 		'.modal-init',
		classInit:  			'init',
		bodyClassOverflow: 		'full-overflow',
		duration: 		  		800,
		tReset: 				null
	};

	/*
		METHODS
	*/
	const removeModal = function(){
		let element = document.querySelector(CONFIG.selectorModal);
		let parent = element.parentNode;
		
		setTimeout(function () {
			document.body.classList.add(CONFIG.classInit);
			setTimeout(function () {
				parent.removeChild(element);
				document.body.classList.remove(CONFIG.bodyClassOverflow);
			}, CONFIG.duration/2);
		}, CONFIG.duration/2);

	};

	const eventCM = function(){
		let togglerNav = document.querySelector('.toggle-nav');
		togglerNav.addEventListener('click' , function(e){
			e.preventDefault();
			let cm = document.querySelector('.c-m');
			cm.setAttribute('href', 'mailto:paul@roughdesign.it');
			cm.innerText = 'paul@roughdesign.it';
		});
	}

	/*
		AUTO INIT
	*/
	const init = (function () {
		
		removeModal();
		eventCM();

	})();

	/*
		METHOD GOES PUBLIC
	*/
	return {
		
	};

})(_R);


