/*
** ========================================================================
**  Switch view
** ========================================================================
	Quick'n'dirty switch between view
** ========================================================================
*/


_R.switchView = (function(_R){
	'use strict';
	/*
		CONSTANTS
	*/
	const CONFIG = {
		selector : 				'[data-view]',
		dataView: 				'data-view',
		dataUrl: 				'href',
		classActive:  			'active',
		bodyClass: 				'page-out',
		transitionTime: 		800,
		tReset: 				null
	};

	/*
		METHODS
	*/
	const switchView = function(e){
		e.preventDefault();
		let el = this;
		el.classList.add(CONFIG.classActive);
		document.body.classList.add(CONFIG.bodyClass);
		setTimer(el);
	}

	const setTimer = function(el){
		CONFIG.tReset = setTimeout(function () {
			window.location.href = el.url;
		}, CONFIG.transitionTime);
	}
	
	/*
		AUTO INIT
	*/
	const init = (function () {

		let elements = document.querySelectorAll(CONFIG.selector);
		let i = 0;
		for (i = elements.length; i--;) {
		  let el = elements[i];
		  el.view = el.getAttribute(CONFIG.dataView);
		  el.url = el.getAttribute(CONFIG.dataUrl);
		  el.addEventListener('click' , switchView);
		}

		window.onbeforeunload = function(e) {
			document.body.classList.add(CONFIG.bodyClass);
		}

	})();

	/*
	TO DO
	- add controll to change location (ex. back or next on browser)
	*/

	/*
		METHOD GOES PUBLIC
	*/
	return {
		
	};

})(_R);


