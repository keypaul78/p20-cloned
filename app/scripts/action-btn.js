/*
** ========================================================================
**  Action button
** ========================================================================
	Visible FAB button on scroll (page details)
** ========================================================================
*/
_R.actionBtn = (function(_R){
	'use strict';
	/*
		CONSTANTS
	*/
	const CONFIG = {
		actionBtn: 				null,
		selector : 				'[data-action]',
		dataTarget: 			'data-trigger',
		classShow:  			'show',
		gapScroll: 				0,
		isScrolling: 			false
	};

	/*
		METHODS
	*/
	const checkScrolling = function(){

		if(_R.Utils.getRect(CONFIG.actionBtn.targetEl).top < 0){
			CONFIG.actionBtn.classList.add(CONFIG.classShow);
		} else {
			CONFIG.actionBtn.classList.remove(CONFIG.classShow);
		}
	};
	
	/*
		AUTO INIT
	*/
	const init = (function () {
		CONFIG.actionBtn = document.querySelector(CONFIG.selector);
		if(CONFIG.actionBtn){
			let dataTarget = CONFIG.actionBtn.getAttribute(CONFIG.dataTarget);
			console.log(dataTarget);
			CONFIG.actionBtn.targetEl = document.querySelector(dataTarget);
			window.addEventListener('scroll', checkScrolling);
		}
	})();

	/*
		METHOD GOES PUBLIC
	*/
	return {
	};

})(_R);


