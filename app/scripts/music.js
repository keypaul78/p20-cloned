/*
** ========================================================================
**  Music
** ========================================================================
	Audio controlloer for bg music
** ========================================================================
*/


_R.music = (function(_R){
	'use strict';
	/*
		CONSTANTS
	*/
	const CONFIG = {
		selectorAudio:  		'#bg-music',
		opener: 				'[data-music-play]',
		closer: 				'[data-music-stop]',
		toggler: 				'[data-toggle-music]',
		dataAudio: 				'data-audio',
		controls: 				document.querySelector('.audio-controls'),
		controlsPlay:         	'playing',
		controlsShow:         	'show',
		audioPlayer: 			null
		
	};

	/*
		METHODS
	*/

	const setMusicPlay = function(e){
		e.preventDefault();
		let el = this;
		playMusic();
	};

	const setMusicStop = function(e){
		e.preventDefault();
		let el = this;
		stopMusic();
	};

	const setMusic = function(choice){
		if(choice === 'no'){
			stopMusic();
			localStorage.setItem('roughdesign_choice-music','no');
		}else {
			playMusic();
			localStorage.setItem('roughdesign_choice-music','yes');
		}
	};

	const toggleMusic = function(e){
		e.preventDefault();
		let el = this;
		if(el.classList.contains(CONFIG.controlsPlay)){
			stopMusic();
			localStorage.setItem('roughdesign_choice-music','no');
		}else {
			playMusic();
			localStorage.setItem('roughdesign_choice-music','yes');
		}
	};

	const playMusicControl = function(){
		document.body.classList.add('sound-on');
		CONFIG.controls.classList.add(CONFIG.controlsShow);
		CONFIG.controls.classList.add(CONFIG.controlsPlay);
		playMusic();
	};

	const stopMusicControl = function(){
		document.body.classList.remove('sound-on');
		CONFIG.controls.classList.add(CONFIG.controlsShow);
		CONFIG.controls.classList.remove(CONFIG.controlsPlay);
		stopMusic();
	};	

	const playMusic = function(){

		let playPromise = CONFIG.audioPlayer.play();

		if (playPromise !== undefined) {
		    playPromise.then(_ => {
		      	document.body.classList.add('sound-on');
		      	CONFIG.controls.classList.add(CONFIG.controlsShow);
				CONFIG.controls.classList.add(CONFIG.controlsPlay);
		    })
		    .catch(error => {
		      stopMusicControl();
		      console.log('Audio promise does\'t works, just tap on headphone icon to play bg sound!');
		    });
		}
	};


	const stopMusic = function(){
		document.body.classList.remove('sound-on');
		CONFIG.controls.classList.add(CONFIG.controlsShow);
		CONFIG.controls.classList.remove(CONFIG.controlsPlay);
		CONFIG.audioPlayer.pause();
		CONFIG.audioPlayer.currentTime = 0;
	};

	const setVolume = function(player, v){
		let vol = v/100;
		player.volume = vol;
	};

	const playAudio = function(e){
		e.preventDefault();
		if(document.body.classList.contains('sound-on')){
			let el = this;
			let targetPlayer = document.querySelector(el.getAttribute(CONFIG.dataAudio));
			setVolume(targetPlayer, 50);
			targetPlayer.play();
		}
	};
	/*
		AUTO INIT
	*/
	const init = (function () {

		let openers = document.querySelectorAll(CONFIG.opener);
		let i;
		for (i = openers.length; i--;) {
		  let el = openers[i];
		  el.addEventListener('click' , setMusicPlay);
		}

		let closers = document.querySelectorAll(CONFIG.closer);
		let k;
		for (k = closers.length; k--;) {
		  let el = closers[k];
		  el.addEventListener('click' , setMusicStop);
		}

		let toggler = document.querySelector(CONFIG.toggler);
		toggler.addEventListener('click' , toggleMusic);

		let audios = document.querySelectorAll('[' + CONFIG.dataAudio + ']');
		let j;
		for (j = audios.length; j--;) {
		  let el = audios[j];
		  el.addEventListener('click' , playAudio);
		}

		CONFIG.audioPlayer = document.querySelector(CONFIG.selectorAudio);
		setVolume(CONFIG.audioPlayer, 5);

	})();


	/*
		METHOD GOES PUBLIC
	*/
	return {
		playMusic: playMusic,
		stopMusic: stopMusic,
		setMusic: setMusic
	};

})(_R);


