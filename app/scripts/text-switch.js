/*
** ========================================================================
**  Text Switch
** ========================================================================
	Switch betwwen 2 or more text
** ========================================================================
*/

// TO DO - not yet implemented

_R.textSwitch = (function(_R){
	'use strict';
	/*
		CONSTANTS
	*/
	const CONFIG = {
		dataInit:           	'data-txt-init',
		selector: 				'data-text-switch',
		classActive:  			'active',
		timeReset: 				[]
	};

	/*
		METHODS
	*/

	const initTextSwitch = function(el){
		CONFIG.timeReset[el.getAttribute('id')] = null;
		let initText = el.getAttribute(CONFIG.dataInit);
		el.removeAttribute(CONFIG.dataInit);
		el.setAttribute('class', initText);
		activeTimerSwitch(el);
	};

	const activeTimerSwitch = function(el){
		clearTimeout(CONFIG.timeReset[el.getAttribute('id')]);
		CONFIG.timeReset[el.getAttribute('id')] = setTimeout(function () {
			switchText(el);
		}, el.getAttribute('data-time')*1000);
	};

	const switchText = function(el){
		let nextData = el.getAttribute(CONFIG.selector);
		let activeClass = el.getAttribute('class');
		el.setAttribute(CONFIG.selector, activeClass);
		el.setAttribute('class', nextData);
		activeTimerSwitch(el);
	};

	const getClassIndex = function(el){
		const classesArray = el.getAttribute('class').split(' ');
		let i = 0;
		for (i = elements.length; i--;) {
		  let el = elements[i];
		  initTextSwitch(el);
		}
	}

	/*
		AUTO INIT
	*/
	const init = (function () {
		/*
		let elements = document.querySelectorAll('[' + CONFIG.selector + ']');
		let i = 0;
		for (i = elements.length; i--;) {
		  let el = elements[i];
		  initTextSwitch(el);
		}
		*/
	})();

	/*
		METHOD GOES PUBLIC
	*/
	return {
		
	};

})(_R);


