// generated on 2019-07-27 using generator-webapp 4.0.0-6
const { src, dest, watch, series, parallel, lastRun } = require('gulp');
const gulpLoadPlugins = require('gulp-load-plugins');
const browserSync = require('browser-sync');
const del = require('del');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const { argv } = require('yargs');
const data = require('gulp-data');
const rename = require("gulp-rename");

const package = require('./package.json');

const $ = gulpLoadPlugins();
const server = browserSync.create();

const lang = package.lang;
const version = package.version;
const replaceVersion = lang + '_' + version;
const localePath = './app/locale/'+ lang +'.json';
const stageUrl = '';//stage url';
const prodUrl = '';//prod url';
const imagesPath = 'images/';
const videoPath = 'video/';//TO ADD
const soundPath = 'sounds/';//TO ADD
const scriptsPath = 'scripts/';
const stylesPath = 'styles/';

const port = argv.port || 9000;

let isProd = process.env.NODE_ENV === 'prod';

const envArgs = process.argv.slice(3);
isProd = envArgs[0] === '--prod' ? true : false;

const isTest = process.env.NODE_ENV === 'test';
const isDev = !isProd && !isTest;

function views() {
  return src('app/*.pug')
    .pipe($.plumber())
    .pipe(data(file => require(localePath))) //locale
    .pipe($.pug({pretty: true}))
    .pipe(dest('.tmp'))
    .pipe(server.reload({stream: true}));
};

function styles() {
  return src('app/styles/*.scss')
    .pipe($.plumber())
    .pipe($.if(!isProd, $.sourcemaps.init()))
    .pipe($.sass.sync({
      outputStyle: 'expanded',
      precision: 10,
      includePaths: ['.']
    }).on('error', $.sass.logError))
    .pipe($.postcss([
      autoprefixer()
    ]))
    .pipe($.if(!isProd, $.sourcemaps.write()))
    .pipe(dest('.tmp/styles'))
    .pipe(server.reload({stream: true}));
};

function scripts() {
  return src('app/scripts/**/*.js')
    .pipe($.plumber())
    .pipe($.if(!isProd, $.sourcemaps.init()))
    .pipe($.babel())
    .pipe($.if(!isProd, $.sourcemaps.write('.')))
    .pipe(dest('.tmp/scripts'))
    .pipe(server.reload({stream: true}));
};


const lintBase = files => {
  return src(files)
    .pipe($.eslint({ fix: true }))
    .pipe(server.reload({stream: true, once: true}))
    .pipe($.eslint.format())
    .pipe($.if(!server.active, $.eslint.failAfterError()));
}
function lint() {
  return lintBase('app/scripts/**/*.js')
    .pipe(dest('app/scripts'));
};
function lintTest() {
  return lintBase('test/spec/**/*.js')
    .pipe(dest('test/spec'));
};

function html() {
  return src(['app/*.html', '.tmp/*.html'])
    .pipe($.useref({searchPath: ['.tmp', 'app', '.']}))
    .pipe($.if(/\.js$/, $.uglify({compress: {drop_console: true}})))
    .pipe($.if(/\.js$/, rename({suffix: '_' + replaceVersion})))
    .pipe($.if(/\.css$/, $.postcss([cssnano({safe: true, autoprefixer: false})])))
    .pipe($.if(/\.css$/, rename({suffix: '_' + replaceVersion})))
    .pipe($.if(/\.html$/, $.htmlmin({
      //collapseWhitespace: true,
      minifyCSS: true,
      minifyJS: {compress: {drop_console: true}},
      processConditionalComments: true,
      removeComments: true,
      removeEmptyAttributes: true,
      removeScriptTypeAttributes: true,
      removeStyleLinkTypeAttributes: true
    })))
    .pipe($.replace('.css', '_' + replaceVersion + '.css'))
    .pipe($.replace('.js', '_' + replaceVersion + '.js'))
    .pipe(dest('dist'));
}

function images() {
  return src('app/images/**/*', { since: lastRun(images) })
    .pipe($.imagemin())
    .pipe(dest('dist/images'));
};

function videos() {
  return src('app/video/**/*')
    .pipe(dest('dist/video'))
};

function sounds() {
  return src('app/sounds/**/*')
    .pipe(dest('dist/sounds'))
};

function fonts() {
  return src('app/fonts/**/*.{eot,svg,ttf,woff,woff2}')
    .pipe($.if(!isProd, dest('.tmp/fonts'), dest('dist/fonts')));
};

function extras() {
  return src([
    'app/*',
    '!app/*.html',
    '!app/views',
    '!app/blocks',
    '!app/mixins',
    '!app/locale',
    '!app/*.pug'
  ], {
    dot: true
  }).pipe(dest('dist'));
};

function clean() {
  return del(['.tmp', 'dist'])
}

function measureSize() {
  return src('dist/**/*')
    .pipe($.size({title: 'build', gzip: true}));
}


const build = series(
  clean,
  lint,
  parallel(
    series(parallel(views, styles, scripts), html),
    images
  ),
  extras,
  videos,
  sounds,
  fonts,
  replacePathHtml,
  replacePathCss,
  measureSize
);



const stage = series(
  clean,
  
 parallel(
    lint,
    series(parallel(views, styles, scripts), html),
    images
  ),
  extras,
  videos,
  sounds,
  fonts,
  replacePathHtml,
  replacePathCss
);


const prod = series(
  clean,
  lint,
 parallel(
    series(parallel(views, styles, scripts), html),
    images
  ),
  extras,
  videos,
  sounds,
  fonts,
  replaceProdPathHtml,
  replaceProdPathCss
);


function replacePathHtml() {
    let pathUrl = stageUrl;
   return src('dist/*.html')
    .pipe($.replace('assets/images/', pathUrl + imagesPath))
    .pipe($.replace('assets/video/', pathUrl + videoPath))
    .pipe($.replace('scripts/', pathUrl + scriptsPath))
    .pipe($.replace('styles/', pathUrl + stylesPath)) 
    .pipe(dest('dist'));
}


function replacePathCss(path) {
  let pathUrl = stageUrl
   return src('dist/styles/*.css')
    .pipe($.replace('../assets/images', pathUrl + imagesPath))
    .pipe(dest('dist/styles'));
}

function replaceProdPathHtml() {
   return src('dist/*.html')
    .pipe($.replace('assets/images/', prodUrl + imagesPath))
    .pipe($.replace('assets/video/', prodUrl + videoPath))
    .pipe($.replace('scripts/', prodUrl + scriptsPath))
    .pipe($.replace('styles/', prodUrl + stylesPath)) 
    .pipe(dest('dist'));
}


function replaceProdPathCss(path) {
   return src('dist/styles/*.css')
    .pipe($.replace('../assets/images', prodUrl + imagesPath))
    .pipe(dest('dist/styles'));
}




function startAppServer() {

  server.init({
    notify: false,
    port,
    server: {
      baseDir: ['.tmp', 'app'],
      routes: {
        '/node_modules': 'node_modules'
      }
    }
  });

  watch([
    'app/*.html',
    'app/assets/images/**/*',
    '.tmp/assets/fonts/**/*'
  ]).on('change', server.reload);

  watch('app/**/*.pug', views);
  watch('app/styles/**/*.scss', styles);
  watch('app/scripts/**/*.js', scripts);
  watch('app/assets/fonts/**/*', fonts);
}

function startTestServer() {
  server.init({
    notify: false,
    port,
    ui: false,
    server: {
      baseDir: 'test',
      routes: {
        '/scripts': '.tmp/scripts',
        '/node_modules': 'node_modules'
      }
    }
  });

  watch('app/scripts/**/*.js', scripts);
  watch(['test/spec/**/*.js', 'test/index.html']).on('change', server.reload);
  watch('test/spec/**/*.js', lintTest);
}

function startDistServer() {
  server.init({
    notify: false,
    port,
    server: {
      baseDir: 'dist',
      routes: {
        '/node_modules': 'node_modules'
      }
    }
  });
}

let serve;
if (isDev) {
  serve = series(clean, parallel(views, styles, scripts, fonts), startAppServer);
} else if (isTest) {
  serve = series(clean, scripts, startTestServer);
} else if (isProd) {
  serve = series(build, startDistServer);
} 

exports.serve = serve;
exports.build = build;
exports.stage = stage;
exports.prod = prod;
exports.default = build;

